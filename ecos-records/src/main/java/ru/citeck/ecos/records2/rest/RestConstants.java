package ru.citeck.ecos.records2.rest;

public class RestConstants {

    public static final String RECS_BASE_URL_META_KEY = "records-base-url";
    public static final String RECS_USER_BASE_URL_META_KEY = "records-user-base-url";
}
